'use strict'

var gulp = require('gulp');
var sass = require('gulp-sass');
let browserSync = require('browser-sync');

let del = require('del'),
    imagemin = require('gulp-imagemin'),
    uglify = require('gulp-uglify'),
    usemin = require('gulp-usemin'),
    rev = require('gulp-rev'),
    cleanCss = require('gulp-clean-css'),
    htmlmin = require('gulp-htmlmin');

gulp.task('sass', done => {
    gulp
        .src('./css/*.scss')
        .pipe(sass().on('error',sass.logError))
        .pipe(gulp.dest('./css'));
        done();
});

gulp.task('sass:watch', () => {
    gulp
        .watch('./css/*.scss',['sass']);
});

gulp.task('browser-sync', () => {
    var files = ['./*.html', './css/*.css', './images/*.{png, jpg, jpeg, gif}', './js/*.js']
    browserSync.init(files, {
        server: {
        baseDir: './'
        }
    });
});

gulp.task('default', gulp.series('browser-sync','sass:watch'));

gulp.task('copyfonts', done =>{
    gulp
        .src('./node_modules/open-iconic/font/fonts/**')
        .pipe(gulp.dest('./dist/fonts'));
        done();
});

gulp.task('clean', function() {
    return del(['dist']);
});

gulp.task('imagemin', done => {
    gulp.src('./images/**')
    .pipe(imagemin({ optimizationLevel:1, progressive: true, interlaced: true }))
    .pipe(gulp.dest('dist/images'));
    done();
});

gulp.task('usemin', function() {
    return gulp.src('./*.html')
    .pipe(usemin({
        css: [ rev ],
        html: [ function () {return htmlmin({ collapseWhitespace: true });} ],
        js: [ uglify, rev ],
        inlinejs: [ uglify ],
        inlinecss: [ cleanCss, 'concat' ]
    }))
    .pipe(gulp.dest('dist/'));
});

gulp.task('build', gulp.series('clean','copyfonts','imagemin','usemin'));